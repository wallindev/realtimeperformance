// To be able to use ES6 server-side without 
// bundling with Webpack
require('@babel/register');
require('./server');

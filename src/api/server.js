import os from 'os'
import path from 'path'
import util from 'util'
import cors from 'cors'

const app = require('express')()

/*
  * App configuration
  *
  */
const PORT = 3001
const HOST = "localhost"

// Cross-origin resource sharing
app.use(cors())

app.get('/api/cpus', (req, res) => {
  const cpus = os.cpus()
  const totalmem = os.totalmem()
  const freemem = os.freemem()
  // console.log("cpus:", cpus);
  // console.log("totalmem:", totalmem);
  // console.log("freemem:", freemem)
  const stats = {
    cpus,
    totalmem,
    freemem
  }
  // console.log("stats:", util.inspect(stats, true, 5, true))
  res.json(stats)
})

app.listen(PORT, HOST, () => console.log(`Express server listening on http://${HOST}:${PORT}!`))

import React, { Component } from 'react'

import Cpus from './Cpus'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.fetchCpuStats()
  }

  state = {
    cpus: []
  }

  updateInterval = 2000

  componentDidMount = () => {
    // console.log('componentDidMount cpus:', this.state.cpus)
    setInterval(() => {
      this.fetchCpuStats()
    }, this.updateInterval);
  }

  fetchCpuStats = () => {
    fetch('http://localhost:3001/api/cpus')
      .then(res => res.json())
      .then(cpus => this.setState(cpus))
  }

  render = () => {
    // console.log('render cpus:', this.state.cpus)
    return (
      <React.StrictMode>
      <div id="mainContainer" className="container">
        <div className="row">
          {this.state.cpus && <Cpus
            cpus={this.state.cpus}
          />}
        </div>
      </div>
      </React.StrictMode>
    )
  }
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Pie } from 'react-chartjs-2';

const Cpus = ({ cpus }) => {

  const title = "Cpu stats"
  const numCpus = cpus.length
  const relWidthChart = 100 / numCpus
  // console.log('relWidthChart:', relWidthChart)

  const divCpus = cpus && cpus.length > 0 && cpus.map((cpu, index) => {
    const date = new Date();
    const shortDate = date.toUTCString();
    const idCpu = `cpu_${cpu.times.sys}`;
    const idDate = `date_${cpu.times.sys}`;
    const idNumber = `number_${cpu.times.sys}`;
    const idModel = `model_${cpu.times.sys}`;
    const idSpeed = `speed_${cpu.times.sys}`;
    const idChart = `chart_${cpu.times.sys}`;
    const model = cpu.model;
    const speed = cpu.speed;

    const maxSpeed = 5000
    const speedLeft = maxSpeed - speed

    const data = {
      labels: [
        'Speed',
        'Speed left',
        // 'Yellow',
        // 'Green',
        // 'Purple',
        // 'Orange'
      ],
      datasets: [{
          label: 'CPU speeds',
          data: [
            speed,
            speedLeft,
            // 3,
            // 5,
            // 2,
            // 3
          ],
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              // 'rgba(255, 206, 86, 0.2)',
              // 'rgba(75, 192, 192, 0.2)',
              // 'rgba(153, 102, 255, 0.2)',
              // 'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              // 'rgba(255, 206, 86, 1)',
              // 'rgba(75, 192, 192, 1)',
              // 'rgba(153, 102, 255, 1)',
              // 'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
      }]
    }

    const options = {
      legend: {
        display: false
      },
      layout: {
        padding: {
            left: 0,
            right: 0,
            top: 5,
            bottom: 0
        }
      },
      responsive: true
    }

    return <li key={ idCpu } className="cpu">
  <div key={ idChart } style={{ display: 'inline-block', width: `${relWidthChart}%` }}>
    <span key={ idNumber } style={{ display: 'inline-block' }}><strong>CPU #:</strong> { index + 1 }</span>
    <span key={ idDate } className="small" style={{ display: 'none' }}>{ shortDate }</span>
    <span key={ idModel } style={{ display: 'inline-block' }}><strong>Model name</strong><br />{ model }</span>
    <span key={ idSpeed } style={{ display: 'inline-block ' }}><strong>Current CPU speed (in MHz)</strong><br />{ speed }</span>
    <Pie
      data={data}
      options={options}
    />
  </div>
</li>});

  return (
    <div id="cpu-wrap" className="col-sm-12">
      <div className="panel panel-default">
        <div className="panel-heading">
          <h1 className="panel-title">{ title }</h1>
        </div>
        <div className="panel-body">
          <ul id="cpus">
            { divCpus }
          </ul>
        </div>
      </div>
    </div>
  );
};

Cpus.propTypes = {
  cpus: PropTypes.arrayOf(PropTypes.object).isRequired,
};

Cpus.defaultProps = {
  cpus: [],
};

export default Cpus;

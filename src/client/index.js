import React from 'react'
import ReactDOM from 'react-dom'
// import { render } from 'react-dom'

import App from './components/App'

if (module.hot) {
  module.hot.accept()
}

ReactDOM.render(<App />, document.getElementById('App'))
// render(<App />, document.getElementById('App'))

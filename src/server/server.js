import webpack from 'webpack'
import webpackConfig, { MODE, DEVMODE, HOST, PORT } from '../../webpack.config.babel'

import cors from 'cors'
import path from 'path'
import fs from 'fs'

import devMiddleware from 'webpack-dev-middleware'
import hotMiddleware from 'webpack-hot-middleware'

let compiler
let compilerClient
// let compilerServer

// Only in dev mode
if (DEVMODE) {
  compiler = webpack(webpackConfig)
  compilerClient = compiler
  // console.log("compiler:", compiler)
  // console.log("compiler.name:", compiler.name)
  // compilerClient = compiler.compilers.find(compiler => compiler.name === `client-${MODE}`)
  // compilerServer = compiler.compilers.find(compiler => compiler.name === `server-${MODE}`)
}

const express = require('express')
const app = express()

const bundleFileName = getBundleFileName(webpackConfig)
const appTitle = "Performance checker"

// Inital HTML
const HTML = `<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- CSS -->
<link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="/css/styles.css">
<link rel="icon" type="image/x-icon" href="/img/favicon-prompt.gif">

<title>${appTitle}</title>

<!-- JS -->
<script src="/js/${bundleFileName}" async defer></script>
</head>
<body>

<!-- React -->
<div id="App"></div>

</body>
</html>`

/*
  * App configuration
  *
  */
// Cross-origin resource sharing
app.use(cors())

// Static content
app.use(express.static(path.resolve(__dirname, "../../assets")))

// Webpack devServer & hotReloading
if (DEVMODE) {
  app.use(
    devMiddleware(
      compilerClient,
      webpackConfig.devServer
    )
  )
  app.use(
    hotMiddleware(
      compilerClient,
      {
        path: "/__webpack_hmr"
      }
    )
  )
}

app.get('/', (req, res) => {
  res.send(HTML)
})

app.listen(PORT, HOST, () => console.log(`Express server listening on http://${HOST}:${PORT}!`))

/**
 * Functions
 */
// Classical function declaration gets hoisted,
// and we can use it further up in code. Function 
// expressions and fat arrow functions do not hoist.
function getBundleFileName(config) {
  // In DEV mode, simply return output filename in webpack config file
  if (DEVMODE)
    return config.output.filename

  // In PROD mode, read name of physical compiled file (because of hash)
  let bundleFile = null
  try {
    const dir = `${config.output.path}/`
    const bundleFiles = fs.readdirSync(dir)
    // Sort files in reverse chronological order (newest first)
    bundleFiles.sort((a, b) => fs.statSync(dir + b).mtime.getTime() - fs.statSync(dir + a).mtime.getTime())
    bundleFile = bundleFiles[0]
    if (!bundleFile) throw new Error("Bundle file undefined")
  } catch(e) {
    console.error("\nCouldn\'t get client bundle file\n")
    console.error(e)
    console.error("\nDid you run build script (\'npm run build\')?\n")
    process.exit()
  }
  return bundleFile
}
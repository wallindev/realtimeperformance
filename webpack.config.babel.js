const path = require('path')

const inclDir = path.resolve(__dirname, "src/client")
const exclDir = path.resolve(__dirname, "node_modules")

export const MODE = process.env.NODE_ENV || 'development'
export const DEVMODE = MODE === 'development'
export const HOST = "localhost"
export const PORT = 3000

const publicPath = `http://localhost:${PORT}/js/`

const hotUpdate = DEVMODE ? {
  hotUpdateChunkFilename: ".hot/[id].[hash].hot-update.js",
  hotUpdateMainFilename: ".hot/[hash].hot-update.json"
} : null

const devServerStats = {
  all: false,
  colors: true,
  entrypoints: true,
  publicPath: true,
  modules: true,
  maxModules: Infinity,
  errors: true,
  errorDetails: true,
}
const stats = devServerStats
// const stats = 'errors-only'

export default {
  name: "client-development",
  mode: MODE,
  entry: "./src/client",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
    publicPath: "/assets/",
  },
  devServer: DEVMODE ? {
    compress: false,
    publicPath: publicPath,
    stats: devServerStats,
    writeToDisk: false,
    hot: true,
  } : {},
  stats,
  module: {
    rules: [
      // { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
      {
        test: /\.jsx?$/,
        // include: [
        //   path.resolve(__dirname, "src/client")
        // ],
        exclude: [
          path.resolve(__dirname, "node_modules")
        ],
        loader: "babel-loader",
        // options: {
        //   presets: ["@babel/preset-env"]
        // },
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
}